package com.ensapay.payment_service.configuration;

import feign.RequestInterceptor;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class FeignConfig {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return request -> request.header("Authorization", "Bearer " + getKeycloakSecurityContext().getTokenString());
    }

    protected KeycloakSecurityContext getKeycloakSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new IllegalStateException("Cannot set authorization header because there is no authenticated principal");
        } else if (!KeycloakAuthenticationToken.class.isAssignableFrom(authentication.getClass())) {
            throw new IllegalStateException(String.format("Cannot set authorization header because Authentication is of type %s but %s is required", authentication.getClass(), KeycloakAuthenticationToken.class));
        } else {
            KeycloakAuthenticationToken token = (KeycloakAuthenticationToken)authentication;
            KeycloakSecurityContext context = token.getAccount().getKeycloakSecurityContext();
            return context;
        }
    }
}
