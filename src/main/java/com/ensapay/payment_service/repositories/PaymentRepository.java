package com.ensapay.payment_service.repositories;

import com.ensapay.payment_service.models.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;

import java.util.List;
import java.util.Optional;

@RepositoryRestController
public interface PaymentRepository extends MongoRepository<Payment,Long> {
    Optional<List<Payment>> findAllByNumeroCompte(String numeroCompte);
}
