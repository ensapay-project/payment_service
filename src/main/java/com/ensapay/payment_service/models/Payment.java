package com.ensapay.payment_service.models;

import com.ensapay.payment_service.enumerations.PaymentStatus;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "Payment")
public class Payment {

    @Id
    private String id;
    private String numeroCompte;
    private LocalDate date;
    private double soldeFacture;
    private String paymentStatus;


}
