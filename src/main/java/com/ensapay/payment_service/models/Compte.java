package com.ensapay.payment_service.models;

import com.ensapay.payment_service.enumerations.CompteStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Compte {

    @Id
    private String numeroCompte;

    @Positive
    private double solde;

    private String intitule;

    //    @Enumerated(EnumType.STRING)
    private CompteStatus statut;

    //    @CreationTimestamp
    private LocalDate dateDeCreation;

    //    @UpdateTimestamp
    private LocalDate dateUpdate;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String codeSecret;

    //    @ManyToOne
    @JsonIgnoreProperties({"comptes"})
    @JsonIgnore
    private Client client;


}
