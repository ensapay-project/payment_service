package com.ensapay.payment_service.enumerations;

public enum PaymentStatus {

    PAYED,
    REJECTED
}
