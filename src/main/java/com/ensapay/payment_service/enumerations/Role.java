package com.ensapay.payment_service.enumerations;

public enum Role {

    ADMIN,
    AGENT,
    CLIENT
}
