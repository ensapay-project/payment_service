package com.ensapay.payment_service.enumerations;

public enum CompteStatus {

    ACTIVE,
    BLOCKED
}
