package com.ensapay.payment_service.controllers;

import com.ensapay.payment_service.communications.SoldeRequest;
import com.ensapay.payment_service.enumerations.PaymentStatus;
import com.ensapay.payment_service.models.Compte;
import com.ensapay.payment_service.models.Facture;
import com.ensapay.payment_service.models.Payment;
import com.ensapay.payment_service.models.Solde;
import com.ensapay.payment_service.repositories.PaymentRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/payment/api")
@RequiredArgsConstructor
@Api(description = "API pour les opérations de validation de paiement")
public class PaymentController {

    private final PaymentRepository paymentRepository;
    private final  SoldeRequest soldeRequest;

//    /**
//     * This api is just for getting account by numeroCompte .
//     * @param numeroCompte
//     * @return compte
//     */
//    @GetMapping("/{numeroCompte}")
//    @ApiOperation(value = "Récupérer un compte à partir de son numéro")
//    public Compte getCompte(@PathVariable String numeroCompte){
//        return soldeRequest.getCompte(numeroCompte).orElseThrow(
//                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Compte introuvable"));
//    }

    /**
     * This api if for checking the facture balance and total account balance .
     * @param numeroCompte
     * @param montant
     * @return Payment object
     */

    @PostMapping("/{numeroCompte}/check")
    @ApiOperation(value = "Ajouter le solde final après la vérification")
    public ResponseEntity<Boolean> checkFacture(@PathVariable String numeroCompte,
                                                @RequestParam double montant)  {
        Payment payment = Payment.builder()
                .soldeFacture(montant)
                .date(LocalDate.now())
                .numeroCompte(numeroCompte)
                .build();

        Solde solde = Solde.builder()
                .solde(-montant)
                .numeroCompte(payment.getNumeroCompte())
                .date(payment.getDate())
                .build();

        //Sending data to Solde service
        try{
            soldeRequest.setSolde(numeroCompte,solde);
        }catch (Exception e){
            payment.setPaymentStatus(PaymentStatus.REJECTED.name());
            paymentRepository.save(payment);
            return ResponseEntity.ok(false);
        }

        payment.setPaymentStatus(PaymentStatus.PAYED.name());
        paymentRepository.save(payment);

        return ResponseEntity.ok(true);
    }

    /**
     * This api is retrieving payments by account number
     * @param numeroCompte
     * @return List of payments
     */
    @GetMapping("/{numeroCompte}/all-payments")
    @ApiOperation(value = "Récupérer tous les paiements d'un compte à partir de son numéro")
    public List<Payment> getAllPayment(@PathVariable String numeroCompte){
        return paymentRepository.findAllByNumeroCompte(numeroCompte).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Paiements introuvable")
        );
    }

}
