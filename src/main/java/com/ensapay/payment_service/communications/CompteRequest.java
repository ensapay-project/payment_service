//package com.ensapay.payment_service.communications;
//
//import com.ensapay.payment_service.models.Compte;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.swing.text.html.Option;
//import java.util.Optional;
//
//@FeignClient(name = "COMPTE-SERVICE")
//public interface CompteRequest {
//
//    @GetMapping("/api/compte/{numeroCompte}")
//    Optional<Solde> getCompte(@PathVariable String numeroCompte);
//
//    @PostMapping("/api/compte/{numeroCompte}/addsolde")
//    Optional<Solde> setSolde(@PathVariable String numeroCompte, @RequestParam double solde);
//}
