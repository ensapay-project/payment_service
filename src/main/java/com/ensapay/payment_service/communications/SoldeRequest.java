package com.ensapay.payment_service.communications;

import com.ensapay.payment_service.configuration.FeignConfig;
import com.ensapay.payment_service.models.Compte;
import com.ensapay.payment_service.models.Solde;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(name="solde-service",url = "http://localhost:8088/solde/api",configuration = FeignConfig.class)
public interface SoldeRequest {

//    @GetMapping("/{numeroCompte}")
//    Optional<Compte> getCompte(@PathVariable String numeroCompte);

    @PostMapping("/{numeroCompte}/add-solde")
    Optional<Compte> setSolde(@PathVariable String numeroCompte, @RequestBody Solde solde);
}
