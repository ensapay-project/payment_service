cd "${0%/*}"
SERVICE_NAME=payment_service
echo "Building $SERVICE_NAME service"
if [[ "$1" == -t || $2 == -t ]]
then
mvn install package
else
mvn install package -DskipTests
fi
docker build -t $SERVICE_NAME:latest .
if [[ "$1" == -d || "$2" == -d ]]
then
echo "Deploying $SERVICE_NAME service"
docker-compose up
fi
